ruby-qr4r (0.6.2-2) unstable; urgency=medium

  * Team upload.
  * d/p/0002-Add-support-to-Minitest-5.22.patch: fix FTBFS
  * Declare compliance with Debian Policy 4.7.0

 -- Lucas Kanashiro <kanashiro@debian.org>  Mon, 16 Dec 2024 18:05:42 -0300

ruby-qr4r (0.6.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Reorder sequence of d/control fields by cme (routine-update)
  * Set upstream metadata fields: Bug-Database, Repository-Browse.
  * Update minimum version of ruby-rqrcode-core to 1.0~

 -- Pirate Praveen <praveen@debian.org>  Sat, 03 Feb 2024 21:17:46 +0530

ruby-qr4r (0.6.1-1) unstable; urgency=medium

  * New upstream version 0.6.1
  * d/control:
    - bump debhelper-compat to 13
    - Drop X?-Ruby-* fields
  * Drop rqrcode_v1.patch and fix_tests.patch, not needed anymore
  * Add simplify_test_init.patch to load the lib from the tests without
    relative path (for autopkgtest)

 -- Cédric Boutillier <boutil@debian.org>  Mon, 06 Feb 2023 16:55:17 +0100

ruby-qr4r (0.4.1-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Repository.

  [ Cédric Boutillier ]
  * Patch to work with rqrcode v1 (Closes: #970852)
  * Build-depends on ruby-rqrcode-core instead of -rqrcode

 -- Cédric Boutillier <boutil@debian.org>  Thu, 08 Oct 2020 14:52:08 +0200

ruby-qr4r (0.4.1-1) unstable; urgency=medium

  * New upstream version 0.4.1
  * Update packaging
  * use gem install layout

 -- Cédric Boutillier <boutil@debian.org>  Wed, 24 Jun 2020 14:51:29 +0200

ruby-qr4r (0.4.0-2) unstable; urgency=medium

  * Team upload.
  * Source-only upload to unstable to allow migration to testing.
  * d/control (Homepage): Use secure URI.
  * d/copyright (Format): Use secure URI.

 -- Daniel Leidert <dleidert@debian.org>  Wed, 26 Feb 2020 16:04:39 +0100

ruby-qr4r (0.4.0-1) unstable; urgency=medium

  * Upload to Debian, as part of the Debian Ruby Sprint 2020
  * Add salsa-ci config
  * Modernize watch and rules files
  * Drop compat file
  * Add Upstream metadata
  * Use the rake method to run tests
  * Do not clean but do not install .rb~ files
  * Add patch to run tests with minitest

 -- Cédric Boutillier <boutil@debian.org>  Wed, 05 Feb 2020 12:48:13 +0100

ruby-qr4r (0.4.0-0kali2) sana; urgency=medium

  * Rebuild for sana (change XS-Ruby-Versions: all)

 -- Sophie Brun <sophie@freexian.com>  Tue, 30 Jun 2015 15:15:31 +0200

ruby-qr4r (0.4.0-0kali1) kali; urgency=medium

  * Initial release

 -- Sophie Brun <sophie@freexian.com>  Thu, 25 Jun 2015 14:20:25 +0200
